﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Browser
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        // This is the core that will perform most navigation and post processing
        private void NavigateToPage()
        {
            browser.Navigate(textBox1.Text);
        }

        // Checks if the address in incorrect
        private bool RemoteFileExists(string url)
        {
            try
            {
                //Creating the HttpWebRequest
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                //Setting the Request method HEAD, you can also use GET too.
                request.Method = "HEAD";
                //Getting the Web Response.
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                //Returns TRUE if the Status code == 200
                response.Close();
                return (response.StatusCode == HttpStatusCode.OK);
            }
            catch
            {
                //Any exception will returns false.
                return false;
            }
        }

        // This function will start every time you press a key and release it
        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            // If you press enter it will run the NavigateToPage method
            if (e.KeyChar == (char)ConsoleKey.Enter)
            {
                e.Handled = true; // This will suppress the irritating system sound when clicking enter
                if (RemoteFileExists(textBox1.Text))
                {
                    NavigateToPage();
                } else
                {
                    // This will run a google search on the textbox input in case a non-valid link
                    browser.Navigate("https://www.google.com/search?q=%22" + textBox1.Text + "%22");
                }
            }

            if (e.KeyChar == (char)ConsoleKey.Escape)
            {
                e.Handled = true;
                browser.Stop();
            }
        }

        // This will Cage things up
        private void cageItUpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (HtmlElement image in browser.Document.Images)
                {
                    image.SetAttribute("src", "https://staticdelivery.nexusmods.com/mods/1151/images/5213-1-1449656129.png");
                }
            } catch (NullReferenceException)
            {
                browser.Navigate("https://staticdelivery.nexusmods.com/mods/1151/images/5213-1-1449656129.png");
            }
        }

        // Manages the way how progressbar works
        private void webBrowser1_ProgressChanged(object sender, WebBrowserProgressChangedEventArgs e)
        {
            try
            {
                if (e.CurrentProgress > 0 && e.MaximumProgress > 0)
                {
                    progressBar1.Value = (int)(e.CurrentProgress * 100 / e.MaximumProgress);
                }
                else if (progressBar1.Value > 100)
                {
                    progressBar1.Value = 100;
                }
                else
                {
                    progressBar1.Value = 0;
                }
            } catch (Exception)
            {
                Console.WriteLine("Something went wrong");
            }
        }

        private void back_Click(object sender, EventArgs e)
        {
            browser.GoBack();
        }

        private void forward_Click(object sender, EventArgs e)
        {
            browser.GoForward();
        }

        private void refresh_Click(object sender, EventArgs e)
        {
            browser.Refresh();
        }

        private void homeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            browser.GoHome();
        }

        private void goButton_Click(object sender, EventArgs e)
        {
            if (RemoteFileExists(textBox1.Text))
            {
                NavigateToPage();
            }
            else
            {
                // This will run a google search using the textbox input in case a non-valid link
                browser.Navigate("https://www.google.com/search?q=%22" + textBox1.Text + "%22");
            }
        }
    }
}
